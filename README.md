# Little intro
I never before was trying to create any sort of extensions and never used js.
Time to make some changes.

Well, in this repo i was trying to create a Firefox extesion that collect Google Music Library on the background and let me to share my library with other people.

# What done:

1) Music data fetching (track name, artist, album and image).
2) Caching data to the browser's storage.
3) Lost my Google Music subscription and froze this project.

# What to do:

1) Sort music by different sources (albums, artists and etc)
2) Basic UI
3) Music statistics