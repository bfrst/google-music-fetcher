console.log("Google Music Fetcher загружен и готов к работе");
/**
 * Google Play Music имеет такую замечательную особенность, как динамически обновлять страницу
 * по мере того как мы скроллим плейлист вниз/вверх и скрывать элементы списка, которые пользователь
 * не видит. По этому чтобы получить весь список треков мы средствами плагина скроллим страницу,
 * пока не получим полный список треков!
 */

// TODO: сделать разграничение на user_mainAlbum, user_tratataAlbum, user_dubstepAlbum, etc. . .
// TODO: реализовать нормальный update

// Информация о треках хранится здесь, в формате {title, artist, album, index, imageUrl}
var availableTracks = {};
// Информация о треках, которые необходимо записать в библиотеку
var pendingTracks = {};
// Перечислние индексов треков, которые уже находятся в библиотеке
var availableIndexies = {};
// Перечисление индексов треков, которые были получены в текущей сессии
var pendingIndexies = {};
// Количество треков всего
var totalTracksCount = 0;
// Количество треков, информация о которых доступна в библиотеке
var availableTracksCount = 0; 
// Количество треков, информация о которых получена в текущей сессии
var pendingTracksCount = 0;
// Количество треков, информация о которых еще не получена
var notAvailableTracks = 0;
// Флаг, указывающий требуется ли обновление локальной библиотеки
var isWritePending = false;

function log(msg)
{
    console.log(msg);
}

function readLocalLibrary()
{
    var result = browser.storage.local.get("tracks");
    result.then(function(rslt) {
        log("Результат чтения локальной библиотеки:");
        log(rslt);
    });
}

function updateLocalLibrary()
{
    if (pendingTracksCount == 0)
    {
        log("Обновление библиотеки треков не требуется.");
        return;
    }
    log("Обновляю библиотеку треков. . .");
    for (var a = 0; a < pendingTracksCount; a++)
    {
        availableTracks[availableTracksCount] = pendingTracks[a];
        availableTracksCount++;
    }
    pendingTracks = {};
    pendingTracksCount = 0;
    log("Библиотека треков обновлена.");
    log("Записываю изменения в локальную библиотеку. . .");
    var result = browser.storage.local.set(
    {
        "tracks": availableTracks
    });
    function onSuccess()
    {
        log("Запись треков в локальную библиотеку успешен.");
    }
    function onError()
    {
        log("Ошибка записи треков в локальную библиотеку.");
    }
    result.then(onSuccess, onError);
}

function purgeLocalLibrary()
{
    log("Очистка локальной библиотеки. . .");
    try {
        browser.local.clear();
    } catch(exc) {
        // try-catch нужен для того, чтобы плагин не крашился при попытке удалить отсутствующие файлы
        //log(exc);
    }
     finally {
        log("Локальная библиотека очищена.");
    }
}

function doHaveCollision(index)
{
    for (a = 0; a < pendingTracksCount; a++)
    {
        if (pendingTracks[a].index == index) return true;
    }
    for (a = 0; a < availableTracksCount; a++)
    {
        if (availableTracks[a].index == index) return true;
    }
    return false;
}

function fetchAvailableTracks()
{
    log("Получаю список доступных треков. . .");
    // результат поиска "song-table"- table[0]->tdbody[1]->получаем количество треков в альбоме
    var totalTracksCount = document.body.getElementsByClassName("song-table")[0].children[1].getAttribute("data-count");
    log("Общее количество доступных треков в библеотеке Google: " + totalTracksCount);
    var visibleTracks = document.body.getElementsByClassName("song-row");
    for (var a = 0; a < visibleTracks.length; a++)
    {
        var index = visibleTracks[a].getAttribute("data-index");
        if (doHaveCollision(index))
        {
            //log("Возникла коллизия, запись не осуществляется.");
            continue;
        }
        var title = visibleTracks[a].getElementsByClassName("column-content")[0].children[0].nextSibling.data;
        if (title == undefined)
        {
            //log("Неверное имя трека, запись не осуществляется");
            continue;
        }
        var artist =  visibleTracks[a].getElementsByClassName("column-content")[1].children[0].firstChild.data;
        var album =  visibleTracks[a].getElementsByClassName("column-content")[2].children[0].firstChild.data;
        var imageUrl = visibleTracks[a].getElementsByClassName("column-content")[0].children[0].getAttribute("src");
        pendingTracks[pendingTracksCount] = {"title": title, "artist": artist, "album":album, "index": index, "imageUrl": imageUrl};
        pendingTracksCount++;
        //log("Получена следующая песня: " + artist + ", " + title + ", " + album + ", " + index);
    }
    //log("Полный список полученных песен: ");
    //log(pendingTracks);
    updateLocalLibrary();
}

function update()
{
    log("Обновление. . .");
    fetchAvailableTracks();
    //setTimeout(update, 12000);
}

purgeLocalLibrary();
update();
setTimeout(update, 5000);
setTimeout(readLocalLibrary, 10000);